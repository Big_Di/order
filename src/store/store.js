import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);


const store = new Vuex.Store({
   state: {
       orders: [],
       lastPrice: {
           lastPriceBTC: 100,
           lastPriceUSD: 4000,
       },
       balance: {
           balanceBTC: 1000,
           balanceUSD: 10000,
       },

       commission: {
           commissionBTC: 0.1,
           commissionUSD: 0.2,
       }

   },

   mutations: {
       pushOrder(state, item) {
            state.orders.push(item);
       },
       removeOrder(state, idx) {
           state.orders.splice(idx, 1);
       },

   },

   actions: {
        addOrders({commit}, item) {
            commit('pushOrder', item);
        },

        removeOrderByPosition({commit}, idx) {
            commit('removeOrder', idx);
        },

    }
});

export default store